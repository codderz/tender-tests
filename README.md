###Tender.pro testing with behat


Запуск тестов по tender.pro

чтобы запустить тесты, нужно:
 
 - сделать клон репозитория
 - **composer update** из папки проекта
  - **bin/behat --tags tender** - запуск тестов
 
 
 
 - **bin/behat** для запуска всех тестов
 - **bin/behat --tags %tag_name%** для запуска тестов по тэгу tag_name
 - **bin/behat --help** отображает справку по Behat

Проверено на php версии 7.1.8