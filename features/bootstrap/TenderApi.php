<?php


use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use GuzzleHttp\Exception\RequestException;

class TenderApi

{
    public $client;
    public $authClient;

    public $request;
    private $access_token;

    public function __construct()
    {
        $stack = HandlerStack::create();

        $this->access_token = "";


        $middleware = new Oauth1([
            'consumer_key' => '',
            'consumer_secret' => '',
            'token' => '',
            'token_secret' => ''
        ]);

        $stack->push($middleware);

        $this->client = new Client([
            'base_uri' => 'http://www.tender.pro/api/',
            'handler' => $stack,
            'auth' => '',
            'debug' => false
        ]);
    }

    public function performGetRequest($uri, $queryOptions = [])
    {
        $options = ['query' => $queryOptions];
        if (isset($options['query']['access_token'])) {
            if ($options['query']['access_token'] == "%CORRECT%") {
                $options['query']['access_token'] =
                    $this->access_token;
            }
        }
        try {
            return $this->client->get($uri, $options);
        } catch (RequestException $e) {
            return $e->getResponse();
        }
    }


    public function performPostRequest($uri, $queryOptions = [])
    {
        $options = ['query' => $queryOptions];

        if (isset($options['query']['access_token'])) {
            if ($options['query']['access_token'] == "%CORRECT%") {
                $options['query']['access_token'] =
                    $this->access_token;;
            }
        }

        try {
            return $this->client->post($uri, $options);
        } catch (RequestException $e) {
            return $e->getResponse();
        }
    }

}

