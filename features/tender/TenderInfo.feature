Feature: tender info testing

  Пример запроса информации о тендере по id компании и id тендера
  с проверкой полей на правильность отображаемых данных


  @tender @tinfo01
  Scenario: tender.info route positive result
    When get "_tender.info.json" with:
    """
  {
  "_key":"1732ede4de680a0c93d81f01d7bac7d1",
  "company_id":"44441",
  "id":"144276"
  }
    """
    Then status code must be 200
    Then check response result.data exists
    #захардкоженая проверка результата
    Then check response "result.data.company_name" has "Краском"
    Then check response "result.data.close_date" has "24.04.2013"

    Then check response "result.data.full_address" has "660049"
    Then check response "result.data.full_address" has "Красноярск"
    Then check response "result.data.full_address" has "ул. Парижской Коммуны, 41"

    Then check response "result.data.ship_date" has "30.05.2013"
    Then check response "result.data.open_date" has 1
    Then check response "result.data.id" has "144276"
    Then check response "result.data.face_email" has "sommer@kraskom.com"
    Then check response "result.data.status_id" has "4"
    Then check response "result.data.winners_list" has "Ленаро Красноярск"
    Then check response "result.data.company_id" has 44441
    Then check response "result.data.is_priv" has 0
    Then check response "result.data.offers_count" has "2"
    Then check response "result.data.offers_count" has "0"
    Then check response "result.data.face_phone" has "391"
    Then check response "result.data.face_phone" has "252-87-37"
    Then check response "result.data.face_name" has "Соммер Татьяна Викторовна"

    Then check response "result.data.title" has "Запрос предложений по 223 ФЗ"
    Then check response "result.data.title" has "КрасКом"
    Then check response "result.data.title" has "id144276"


