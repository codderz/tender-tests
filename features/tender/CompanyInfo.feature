Feature: tender testing
  @tender @compinfo01
  Scenario: company.info route positive result
    When get "_company.info_public.json" with:
    """
  {
  "id":"80034"
  }
    """
    Then status code must be 200
    Then check response "result.args.id" equals "result.data.id"
    Then check response "result.data.address_legal" has "654007, Россия, Новокузнецк, Н.С. Ермакова, 9, а"
    Then check response "result.data.title_full" has "Акционерное общество"
    Then check response "result.data.title_full" has "ТопПром"
    Then check response "result.data.anno_short" has "Переработка и поставки угольной продукции"
    Then check response "result.data.rating" has "2.000"

    Then check response "result.data.is_seller_producer" has 1
    Then check response "result.data.country_id" has "1"
    Then check response "result.data.is_type_seller" has 0
    Then check response "result.data.address" has "654007"
    Then check response "result.data.address" has "Кемеровская область, Новокузнецк, Н.С. Ермакова, 9, а"
    Then check response "result.data.type_name" has "организатор"
    Then check response "result.data.country_name" has "Россия"
    Then check response "result.data.phone" has "99-37-33"
    Then check response "result.data.phone" has "3843"

    Then check response "result.data.seller_type_name" has "производитель"
    Then check response "result.data.kpp" has 421650001
    Then check response "result.data.inn" has 4220020218
    Then check response "result.data.site" has "www.top-prom.ru"
    Then check response "result.data.title" has "ТопПром"




