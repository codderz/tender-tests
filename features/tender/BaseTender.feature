Feature: tender route testing
  @tender @bst @base00
  Scenario: Запрос списка тендеров, затем по первому их них получение информации о тендере и о компании
    When tender lists performed
    Then status code must be 200
    Then check response result.data exists
    Then check response result.data count equals to 3
    Then check response "result.data.0.company_name" has "Русское Зерно Уфа"
    Then check response "result.data.0.company_id" has "203102"
    Then check response "result.data.0.id" has "289589"

    #tender info выполняется с сохраненными параметрами id, company_id из tender_list
    When tender info performed
    Then status code must be 200
    Then check response "result.data.company_name" equal session data "company_name"
    Then check response "result.data.company_id" equal session data "company_id"
    Then check response "result.data.id" equal session data "tender_id"

    #company info выполняется с сохраненными параметрами id, company_id из tender_list
    When company info performed
    Then status code must be 200
    Then check response "result.data.title_full" equal session data "company_name"
    Then check response "result.data.title" equal session data "company_name"
    Then check response "result.data.id" equal session data "company_id"


  @tender @bst @base01
  Scenario: Получить список первых 3 тендеров в состоянии открыт для любой компании-организатора
    When get "_info.tenderlist_by_set.json" with:
    """
  {
  "_key":"1732ede4de680a0c93d81f01d7bac7d1",
  "set_type_id":"2",
  "set_id":"7964",
  "max_rows":"3",
  "open_only":"t"
  }
    """
    Then status code must be 200
    Then check response result.data exists
    Then check response result.data count equals to 3
    Then check response "result.data.0.company_name" has "Русское Зерно Уфа"
    Then check response "result.data.0.company_id" has "203102"
    #id тендера
    Then check response "result.data.0.id" has "289589"

  @tender  @bst @base02
  Scenario: Получить детальную инфо о тендере (возьмем id первого тендера и id компании из результата предыдущего запроса)
    When get "_tender.info.json" with:
    """
  {
  "_key":"1732ede4de680a0c93d81f01d7bac7d1",
  "company_id":"203102",
  "id":"289589"
  }
    """
    Then status code must be 200
    Then check response "result.data.company_name" has "Русское Зерно Уфа"
    Then check response "result.data.company_id" has "203102"


  @tender @bst @base03
  Scenario: получаем инфо о компании (id компании из результата запроса списка тендеров)
    When get "_company.info_public.json" with:
    """
  {
  "id":"203102"
  }
    """
    Then status code must be 200
    Then check response "result.data.title_full" has "Русское Зерно Уфа"
    Then check response "result.data.id" has "203102"
    Then check response "result.args.id" equals "result.data.id"


  @tender @bst @base04
  Scenario:
    When get "_tender.info.json" with:
    """
  {
  "_key":"1732ede4de680a0c93d81f01d7bac7d1",
  "company_id":"1",
  "id":"289589"
  }
    """
    Then status code must be 200
    Then check response "result.args.id" equals "result.data.id"
    Then check response "result.data.company_name" has "Русское Зерно Уфа"
    Then check response "result.data.company_id" has "203102"
    Then check response "result.data.id" has "289589"